function startApp() {
    let start = prompt(`input event name`, `meeting`);
    if (start === `meeting`) {
        createForm()
    }
}

function createForm() {
    const content = document.getElementById(`content`);

    let mainDiv = document.createElement(`div`);
    mainDiv.setAttribute(`class`, `main`);

    let inputName = document.createElement(`input`);
    setAttributes(inputName, {
        'class': 'input', 'type': 'text',
        'name': 'name', 'id': 'name', 'placeholder': ' Name...'
    });
    let inputTime = document.createElement('input');
    setAttributes(inputTime, {
        'class': 'input', 'type': 'text',
        'name': 'time', 'id': 'time', 'placeholder': ' Time...'
    });
    let inputPlace = document.createElement('input');
    setAttributes(inputPlace, {
        'class': 'input', 'type': 'text',
        'name': 'place', 'id': 'place', 'placeholder': ' Place...'
    });
    let nameLabel = document.createElement('div');
    nameLabel.innerHTML = '<p>Enter name:</p>';
    setAttributes(nameLabel, { 'class': 'label' });
    let timeLabel = document.createElement('div');
    timeLabel.innerHTML = '<p>Enter time:</p>';
    setAttributes(timeLabel, { 'class': 'label' });
    let placeLabel = document.createElement('div');
    placeLabel.innerHTML = '<p>Enter place:</p>';
    setAttributes(placeLabel, { 'class': 'label' });

    let btnWrapper = document.createElement('div');
    setAttributes(btnWrapper, { 'class': 'btnWrapper' });
    let btnSubmit = document.createElement('button');
    setAttributes(btnSubmit, { 'class': 'btn', 'type': 'submit', 'id': 'submit' });
    btnSubmit.innerText = 'Confirm';
    let btnConverter = document.createElement('button');
    setAttributes(btnConverter, { 'class': 'btn', 'id': 'converter' });
    btnConverter.innerText = 'Converter';
    appendSeveralChilds(btnWrapper, [btnSubmit, btnConverter]);


    appendSeveralChilds(mainDiv, [nameLabel, inputName, timeLabel, inputTime, placeLabel, inputPlace, btnWrapper])
    content.appendChild(mainDiv);

    btnSubmit.addEventListener(`click`, validateInputs);
    btnConverter.addEventListener(`click`, startConverter);

    function validateInputs() {
        if (inputName.value.length === 0 || inputTime.value.length === 0 || inputPlace.value.length === 0) {
            alert('Input all data');
        } else {
            validateTime();
        }
    }

    function validateTime() {
        if (inputTime.value.match(/^(2[0-3]|[01]?[0-9]):([0-5]?[0-9])$/)) {
            console.log(`${inputName.value} has a meeting today at ${inputTime.value}
             somewhere in ${inputPlace.value}`);
        } else {
            alert('Enter time in format hh:mm');
        }
    }

    function startConverter() {
        const coursOfEuro = 32.98;
        const coursOfDollar = 27.68;

        let euroToUah;
        let dollarToUah;
        let amountOfEuro = prompt(`input amount of euro`);
        let amountOfDollar = prompt(`input amount of dolar`);
        let maxNull = 2;

        if (amountOfEuro > 0 && amountOfDollar > 0
        ) {
            euroToUah = amountOfEuro * coursOfEuro;
            dollarToUah = amountOfDollar * coursOfDollar;
            console.log(`${amountOfEuro} euros are equal ${euroToUah.toFixed(maxNull)} hrns,
                 ${amountOfDollar} dollars are equal ${dollarToUah.toFixed(maxNull)} hrns`)
        } else {
            alert('Enter correct value');
        }
    }
}

function setAttributes(el, attributes) {
    Object.keys(attributes).forEach(function (name) {
        el.setAttribute(name, attributes[name]);
    })
}

function appendSeveralChilds(el, child) {
    for (let i = 0; i < child.length; i++) {
        el.appendChild(child[i])
    }
}

startApp()