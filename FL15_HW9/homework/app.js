function reverseNumber(num) {
    let numArrey = num.toString();
    let newString = '';

    for (let i = numArrey.length; i >= 0; i--) {
        newString += numArrey.charAt(i);
    }
    console.log(+newString);
}

function forEach(arr, func) {
    for (el of arr) {
        func(el);
    }
}

function map(arr, func) {
    let newArrey = [];
    function pushToArr() {
        newArrey.push(func(el));
    }
    forEach(arr, pushToArr);
    return newArrey;
}

function filter(arr, func) {
    let newArrey = [];
    function compareElements() {
        if (func(el) === true) {
            let i = el;
            newArrey.push(i);
        }
    }
    forEach(arr, compareElements);
    return newArrey;
}

function getAdultAppleLovers(data) {
    function searchProperty(data) {
        let age = data.age;
        let fruit = data.favoriteFruit;
        let minimalAge = 18;
        if (age > minimalAge && fruit === 'apple') {
            return true;
        }
    }
    map(filter(data, searchProperty), function (el) {
        return el.name;
    })
}

function getKeys(obj) {
    let result = [];

    for (key in obj) {
        if (obj.hasOwnProperty(key)) {
            result.push(key.toString());
        }
    }
    console.log(result);
    return result;
}

function getValues(obj) {
    let result = [];
    for (key in obj) {
        if (obj.hasOwnProperty(key)) {
            result.push(obj[key]);
        }
    }
    console.log(result);
    return result;
}

function showFormattedDate(dateObj) {
    let day = dateObj.getDate();
    let options = { month: 'long' };
    let maxLengthOfMonth = 3;
    let month = new Intl.DateTimeFormat('en-US', options).format(dateObj).slice(0, maxLengthOfMonth);
    let year = dateObj.getFullYear();
    let result = `It is ${day} of ${month}, ${year}`;
    console.log(result);
    return result;
}

