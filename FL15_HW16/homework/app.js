const root = document.getElementById('root');
const modifyItem = document.getElementById('modifyItem');
const addTweet = document.querySelector('.addTweet');
const tweetItems = document.getElementById('tweetItems');

addTweet.addEventListener('click', addNewTweet);
window.addEventListener('hashchange', checkHash);

function start(){
    if (localStorage.length === 0){
        modifyItem.classList.add('display-none');
    }
}

start();

function checkHash(){
    if (window.location.hash === '#add'){
        tweetItems.classList.add('display-none');
        modifyItem.classList.remove('display-none');
    } else {
        tweetItems.classList.toggle('display-none');
        modifyItem.classList.toggle('display-none');
    }
}

function addNewTweet(){
    window.open('#add', '_parent');
}