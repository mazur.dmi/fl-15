function startApp() {

    const appRoot = document.getElementById('app-root');
    const listOfRegion = externalService.getRegionsList();
    const listOfLanguge = externalService.getLanguagesList();

    let form = document.createElement('form');
    form.setAttribute(`class`, `form_header`);

    let formHeader = document.createElement(`h1`);
    formHeader.innerText = `Countries Search`;

    let fields = document.createElement(`div`);
    fields.setAttribute('class', 'fields')
    let fieldOneTitle = document.createElement(`div`);
    fieldOneTitle.innerText = `Please choose the type of search:`;

    let fieldOneRadio = document.createElement(`div`);

    let radioOne = document.createElement(`div`);
    radioOne.setAttribute('class', 'radio_div')
    let radioRegion = document.createElement(`input`);
    let labelRegion = document.createElement(`label`);
    labelRegion.setAttribute('for', 'radioRegion')
    labelRegion.innerText = 'By Region';

    setAttributes(radioRegion,
        {
            'type': 'radio',
            'id': 'region',
            'value': 'region',
            'name': 'check'
        })

    appendSeveralChilds(radioOne, [radioRegion, labelRegion]);

    let radioTwo = document.createElement(`div`);
    radioTwo.setAttribute('class', 'radio_div');
    let radioLanguage = document.createElement(`input`);
    let labelLanguage = document.createElement(`label`);
    labelLanguage.setAttribute('for', 'radioLanguage');
    labelLanguage.innerText = 'By Language';

    setAttributes(radioLanguage,
        {
            'type': 'radio',
            'id': 'language',
            'value': 'language',
            'name': 'check'
        })

    appendSeveralChilds(radioTwo, [radioLanguage, labelLanguage])
    appendSeveralChilds(fieldOneRadio, [radioOne, radioTwo])
    appendSeveralChilds(fields, [fieldOneTitle, fieldOneRadio])


    let selectPart = document.createElement(`div`);
    selectPart.setAttribute('class', 'select');
    let fieldTwoTitle = document.createElement(`div`);
    fieldTwoTitle.innerText = `Please choose search query: `;

    let selectInput = document.createElement(`div`);
    let select = document.createElement(`select`);
    select.innerHTML = `<option>select value</option>`;
    setAttributes(select,
        {
            'size': '1',
            'id': 'select',
            'disabled': ''
        })

    selectInput.appendChild(select);
    appendSeveralChilds(selectPart, [fieldTwoTitle, selectInput]);
    appendSeveralChilds(form, [formHeader, fields, selectPart]);

    appRoot.appendChild(form);

    const radioButton = document.getElementsByName('check');
    let checkedValue;

    radioButton[0].addEventListener('click', changeSelectList);
    radioButton[1].addEventListener('click', changeSelectList);

    let tableLabel;

    function changeSelectList(value) {
        select.disabled = false;

        if (!tableLabel) {
            tableLabel = document.createElement('div');
            tableLabel.setAttribute('class', 'tabel_label');
            tableLabel.innerText = `No items, please choose search query`;
            appRoot.appendChild(tableLabel);
        }

        checkedValue = value.target.value;
        let opt;
        for (i = select.length - 1; i > 0; i--) {
            select.options[i] = null;
        }

        if (checkedValue === 'region') {
            for (i = 0; i < listOfRegion.length; i++) {
                opt = document.createElement('option');
                opt.value = listOfRegion[i];
                opt.innerHTML = `${listOfRegion[i]}`;
                select.appendChild(opt);
            }
        }

        if (checkedValue === 'language') {
            for (i = 0; i < listOfLanguge.length; i++) {
                opt = document.createElement('option');
                opt.value = listOfLanguge[i];
                opt.innerHTML = `${listOfLanguge[i]}`;
                select.appendChild(opt);
            }
        }
    }

    select.addEventListener('change', createTable);
    let table;

    function createTable() {
        let countryListByLanguge = externalService.getCountryListByLanguage(this.options[this.selectedIndex].text);
        let countryListByRegion = externalService.getCountryListByRegion(this.options[this.selectedIndex].text);
        let name,
            flagURL,
            region,
            area,
            capital,
            languages

        let listLength;
        if (countryListByLanguge.length !== 0) {
            listLength = countryListByLanguge.length;
        } else
            if (countryListByRegion !== 0) {
                listLength = countryListByRegion.length;
            }

        thList = ['Country name', 'Capital', 'World region', 'Languages', 'Area', 'Flag'];

        table = document.createElement('table');
        let sortButton = document.createElement('button');
        sortButton.setAttribute('class', 'button_sort_to_up');
        sortButton.addEventListener('click', () => {
            sortTable(0),
                arrowChangeName()
        });
        let sortButtonArea = document.createElement('button');
        sortButtonArea.setAttribute('class', 'button_sort_to_up');
        sortButtonArea.addEventListener('click', () => {
            sortTable(4),
                arrowChangeArea()
        });


        table.setAttribute('id', 'table');
        for (i = 0; i < thList.length; i++) {
            let th = document.createElement('th');
            th.setAttribute('class', 'th');
            th.innerText = `${thList[i]}`;
            if (i === 0) {
                th.setAttribute(`id`, `country_name`);
                th.appendChild(sortButton);
            }
            if (i === 4) {
                th.setAttribute(`id`, `area`);
                th.appendChild(sortButtonArea);
            }
            table.appendChild(th);
        }

        let td, tr;
        for (i = 0; i < listLength; i++) {
            tr = document.createElement('tr');
            tr.setAttribute('class', 'tr');

            if (listLength === countryListByLanguge.length) {
                name = countryListByLanguge[i].name;
                flagURL = countryListByLanguge[i].flagURL;
                region = countryListByLanguge[i].region;
                area = countryListByLanguge[i].area;
                capital = countryListByLanguge[i].capital;
                languages = Object.values(countryListByLanguge[i].languages)
            }

            if (listLength === countryListByRegion.length) {
                name = countryListByRegion[i].name;
                flagURL = countryListByRegion[i].flagURL;
                region = countryListByRegion[i].region;
                area = countryListByRegion[i].area;
                capital = countryListByRegion[i].capital;
                languages = Object.values(countryListByRegion[i].languages);
            }

            table.appendChild(tr);

            for (let j = 0; j < thList.length; j++) {
                td = document.createElement('td');
                td.setAttribute('class', 'td');

                if (j === 0) {
                    td.innerText = `${name}`;
                }
                if (j === 1) {
                    td.innerText = `${capital}`;
                }
                if (j === 2) {
                    td.innerText = `${region}`;
                }
                if (j === 3) {
                    td.innerText = `${languages.toString().replace(/,/g, `, `)}`;
                }
                if (j === 4) {
                    td.innerText = `${area}`;
                }
                if (j === 5) {
                    td.innerHTML = `<img src=${flagURL} alt="flag">`;
                }

                tr.appendChild(td);
            }
        }

        tableLabel.classList.add(`display-none`);

        if (!appRoot.childNodes[2]) {
            appRoot.appendChild(table);
        } else {
            let tableRemove = document.getElementById('table');
            tableRemove.remove();
            appRoot.appendChild(table);
        }
        return appRoot.appendChild(table);

        function arrowChangeName() {
            sortButton.classList.toggle('arrow_to_down');
        }
        function arrowChangeArea() {
            sortButtonArea.classList.toggle('arrow_to_down');
        }
    }




    function sortTable(n, table) {
        table = document.getElementById('table');
        let rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;

        switching = true;
        dir = 'asc';
        while (switching) {
            switching = false;
            rows = table.rows;

            for (i = 0; i < rows.length - 1; i++) {
                shouldSwitch = false;
                x = rows[i].getElementsByTagName('td')[n];
                y = rows[i + 1].getElementsByTagName('td')[n];

                if (dir === 'asc') {
                    if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                        shouldSwitch = true;
                        break;
                    }
                } else if (dir === 'desc') {
                    if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                        shouldSwitch = true;
                        break;
                    }
                }
            }
            if (shouldSwitch) {
                rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
                switching = true;
                switchcount++;
            } else {
                if (switchcount === 0 && dir === 'asc') {
                    dir = 'desc';
                    switching = true;
                }
            }
        }
    }


}

startApp()



function appendSeveralChilds(el, child) {
    for (let i = 0; i < child.length; i++) {
        el.appendChild(child[i]);
    }
}

function setAttributes(el, attrs) {
    for (let key in attrs) {
        if (Object.prototype.hasOwnProperty.call(attrs, key)) {
            el.setAttribute(key, attrs[key]);
        }
    }
}