

let calculateProfit = function (){
    let amountOfMoney = parseFloat(prompt('input initial amount of money'));
    let numberOfYears = parseInt(prompt('input number of years'));
    let percentOfYear = parseFloat(prompt('input percentage of a year'));
    let moneyValid = false;
    let numberValid = false;
    let percentValid = false;
    let result;
    let profit;
    let amountOfMoneyStart = amountOfMoney;
    const minMoney = 1000;
    const minYears = 1;
    const maxPercent = 100;
    const twoSymbol = 2;


        if (typeof amountOfMoney === 'number' && amountOfMoney >= minMoney){
            moneyValid = true;
        } 
  
        if (typeof numberOfYears === 'number' && numberOfYears >= minYears){
            numberValid = true;
        } 

        if (typeof percentOfYear === 'number' && percentOfYear >= 0 && percentOfYear <= maxPercent){
            percentValid = true;
        } 

        if (moneyValid === true && numberValid === true && percentValid === true){
            for(let i = 0; i < numberOfYears; i++){
               result = amountOfMoney * percentOfYear / maxPercent;
               amountOfMoney = result + amountOfMoney;
               profit = amountOfMoney - amountOfMoneyStart;
            }
            alert(`
            Initial amount: ${amountOfMoneyStart}
            Number of years: ${numberOfYears}
            Percentage of year: ${percentOfYear.toFixed(twoSymbol)}

            Total profit: ${profit.toFixed(twoSymbol)}
            Total amount: ${amountOfMoney.toFixed(twoSymbol)}`);
        } else {
            alert('Invalid input data');
        }  
    };

calculateProfit();
