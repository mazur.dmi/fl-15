const defoult = {
    'firstLevel': {
        'minValue': 0,
        'maxValue': 8,
        'prizeOne': 100,
        'prizeTwo': 50,
        'prizeThree': 25
    },
    'secondLevel': {
        'minValue': 0,
        'maxValue': 12,
        'prizeOne': 200,
        'prizeTwo': 100,
        'prizeThree': 50
    }
}

function startGame() {
    let start = confirm('Do you want to play a game?');
    if (start === true) {
        playCasino(defoult.firstLevel.minValue,
            defoult.firstLevel.maxValue,
            defoult.firstLevel.prizeOne,
            defoult.firstLevel.prizeTwo,
            defoult.firstLevel.prizeThree)
    } else {
        sayBye();
    }
}

function sayBye() {
    alert(`You did not become a billionaire, but can.`);
}

function playCasino(minValue, maxValue, prizeOne, prizeTwo, prizeThree) {
    const startAttempts = 3;
    let attempts = 3;
    const maxAttempts =2;

    let randomNumber = Math.floor(Math.random() * (maxValue - minValue)) + minValue;
    console.log(randomNumber);

    let userNumber;
    let startNew;
    let nextLevel;
    let i;

    for (i = 0; i < startAttempts; i++) {
        userNumber = parseFloat(prompt(`
        Choose a roulette pocket number from ${minValue} to ${maxValue}
        Attempts left: ${attempts--}
        Possible prize on current attempt: ${choosePrize(i)}$`));
        if (userNumber === randomNumber) {
            showResult(choosePrize(i));
            break
        }
        if (i === maxAttempts) {
            toLose();
            startNewGame();
        }
    }

    function showResult(value) {
        nextLevel = confirm(`Congratulation, you won!   Your prize is: ${value}$. Do you want to continue?`);
        if (nextLevel === true) {
            playNextLevelGame();
        } else {
            toWinAndEnd(i);
            startNewGame();
        }
    }

    function choosePrize(i) {
        if (i === 0) {
            return prizeOne;
        }
        if (i === 1) {
            return prizeTwo;
        }
        if (i === maxAttempts) {
            return prizeThree;
        } else {
            return 0;
        }
    }

    function startNewGame() {
        startNew = confirm('Do you want to play again?');
        if (startNew === true) {
            playCasino();
        } else {
            sayBye();
        }
    }

    function toLose() {
        alert(`Thank you for your participation. Your prize is: ${choosePrize()}$`);
    }

    function toWinAndEnd() {
        alert(`Thank you for your participation. Your prize is: ${choosePrize(i)}$`);
    }

    function playNextLevelGame() {
        playCasino(defoult.secondLevel.minValue,
            defoult.secondLevel.maxValue,
            defoult.secondLevel.prizeOne,
            defoult.secondLevel.prizeTwo,
            defoult.secondLevel.prizeThree);
    }
}

startGame()

