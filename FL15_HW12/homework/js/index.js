function visitLink(path) {
	let i = localStorage.getItem(path);
	let newValue = i + 1;
	localStorage.setItem(path, newValue);
}

function viewResults() {
	const content = document.getElementById(`content`);

	let result = document.createElement(`ul`);
	result.setAttribute(`class`, `resultText`);

	let resultItemOne = document.createElement(`li`);
	resultItemOne.setAttribute(`class`, `result_item`);

	let resultItemTwo = document.createElement(`li`);
	resultItemTwo.setAttribute(`class`, `result_item`);

	let resultItemThree = document.createElement(`li`);
	resultItemThree.setAttribute(`class`, `result_item`);

	let pageOne = localStorage.getItem('Page1');
		if (pageOne === null){
			pageOne = 0;
		} else {
			pageOne = pageOne.length;
		}
	
	let pageTwo = localStorage.getItem('Page2');
		if (pageTwo === null){
			pageTwo = 0;
		} else {
			pageTwo = pageTwo.length;
		}

	let pageThree = localStorage.getItem('Page3');
		if (pageThree === null){
			pageThree = 0;
		} else {
			pageThree = pageThree.length;
		}

	resultItemOne.innerText = `You visited Page1 ${pageOne} time(s)`;
	resultItemTwo.innerText = `You visited Page2 ${pageTwo} time(s)`;
	resultItemThree.innerText = `You visited Page3 ${pageThree} time(s)`;


	appendSeveralChilds(result, [resultItemOne, resultItemTwo, resultItemThree]);
	content.appendChild(result);

	localStorage.clear();

	function appendSeveralChilds(el, child) {
		for (let i = 0; i < child.length; i++) {
			el.appendChild(child[i]);
		}
	}
}

