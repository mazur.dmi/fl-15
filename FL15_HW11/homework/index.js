function isEquals(a, b){
    return a === b
}

function isBigger(a, b){
    return a > b
}

function storeNames(){
    let arr = [];
    arr.push(...arguments);
    return arr;
}

function getDifference(a, b){
    let result;
    a > b ?
    result = a - b :
    result = b - a;
    return result;
}

function negativeCount(arr){
    let count = 0;
    for (let i = 0; i < arr.length; i++){
        if(arr[i] < 0) {
            count++;
        }
    }
return count;
}

function letterCount(str1, str2){
    if(typeof str1 === 'string' && typeof str2 === 'string'){
        let count = 0;
        let index = 0;
        for (let i = 1; i < str1.length; i++){
            if (str1.indexOf(str2, index) >= 0){
                count++
            }
            index = str1.indexOf(str2) + i;
        }
        return count;
    }
}

function countPoints(arr){
    let gameResult;
    let x, y, index;
    const win = 3;
    const draw = 1;
    const loose = 0;
    let totalPoints = [];
    for (let i = 0; i < arr.length; i++){
        gameResult = arr[i];
        index = gameResult.indexOf(':');
        x = +gameResult.slice(0, index);
        y = +gameResult.slice(index+1);
        if (x > y){
            totalPoints.push(win);
        }
        if (x === y){
            totalPoints.push(draw);
        }
        if (x < y){
            totalPoints.push(loose);
        }
    }

    const reducer = (accumulator, currentValue) => accumulator + currentValue;

    return totalPoints.reduce(reducer);
}
