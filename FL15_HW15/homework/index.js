/* START TASK 1: Your code goes here */
let tdList = document.getElementsByTagName('td')
for (let i = 0; i < tdList.length; i++) {
    tdList[i].addEventListener('click', changeColor);
}

function changeColor(e) {
    let table = document.getElementsByTagName('table');
    let tdList = table[0].tBodies[0].children
    let td = e.target;
    td.classList.toggle('td_yellow');

    if (td.cellIndex === 0) {

        td.classList.toggle('tr_blue');
        td.nextElementSibling.classList.toggle('tr_blue');
        td.nextElementSibling.nextElementSibling.classList.toggle('tr_blue');
    }
    if (td.innerText === 'Special cell') {
        td.classList.add('table_green');
        for (let i = 0; i < tdList.length; i++) {

            for (let j = 0; j < tdList[i].cells.length; j++) {
                let tdWhite = tdList[i].cells[j];
                if (tdWhite.classList.value === '') {
                    tdWhite.classList.toggle('table_green');
                } else {
                    tdWhite.classList.remove('table_green');
                }
            }
        }
    }
}

/* END TASK 1 */

/* START TASK 2: Your code goes here */

/* END TASK 2 */

/* START TASK 3: Your code goes here */

/* END TASK 3 */
