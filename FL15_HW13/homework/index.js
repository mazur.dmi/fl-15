let today = new Date();

// task 1
function getAge(birthdayDate) {
    let years = today.getFullYear() - birthdayDate.getFullYear();
    let birthdayDay = birthdayDate.getDate();
    let todayDay = today.getDate();
    let birthdayMonth = birthdayDate.getMonth();
    let todayMonth = today.getMonth();
    let result = years;

    if (birthdayMonth > todayMonth || birthdayMonth === todayMonth &&
        birthdayDay >= todayDay) {
        result = years - 1;
    }
    console.log(result);
    return result;
}

// const birthday28 = new Date(2000, 2, 28);
// const birthday30 = new Date(2000, 2, 31);
// getAge(birthday28); // 21 (assuming today is the 30.03)
// getAge(birthday30); // 20 (assuming today is the 30.03)

// task 2
function getWeekDay() {
    let date = new Date(arguments[0]);
    let days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    let result = days[date.getDay()];
    console.log(result);
    return result;
}

// getWeekDay(Date.now()); // "Thursday" (if today is the 22nd October)
// getWeekDay(new Date(2020, 9, 22)); // "Thursday"

// task 3
function getAmountDaysToNewYear() {
    let date = new Date(arguments[0]);
    let newYearDate = new Date(2022, 0, 1);
    let result = Math.floor((newYearDate.getTime() - date.getTime()) / 1000 / 60 / 60 / 24);
    console.log(result);
    return result;
}

// getAmountDaysToNewYear(Date.now()); // 124 (if today is the 30th August)

// task 4
function getProgrammersDay(value) {
    let dayInMilisec = 255 * 24 * 60 * 60 * 1000;
    let yearDate = new Date(value, 0);
    let yearInMilisec = yearDate.getTime();
    let date = new Date(yearInMilisec + dayInMilisec);
    let month = date.toDateString().split(' ')[1];
    let day = getWeekDay(date);
    let result = `${date.getDate()} ${month}, ${date.getFullYear()} (${day})`;
    console.log(result);
    return result;
}

// getProgrammersDay(2020); // "12 Sep, 2020 (Saturday)"
// getProgrammersDay(2019); // "13 Sep, 2019 (Friday)"


// task 5
function howFarIs(value) {
    let inputDay = String(value).toUpperCase();
    let days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    let specifiedWeekday;
    let indexOfDay;
    let result;
    let todayDay = days[today.getDay()];

    for (let i = 0; i < days.length; i++) {
        if (inputDay === days[i].toUpperCase()) {
            indexOfDay = i;
        }
    }

    specifiedWeekday = days[indexOfDay];

    let number
    if (today.getDay() < indexOfDay) {
        number = indexOfDay - today.getDay();
    } else {
        number = indexOfDay + today.getDay();
    }

    if (specifiedWeekday === todayDay) {
        result = `Hey, today is ${specifiedWeekday} =)`
        console.log(result);
        return result;
    } else {
        result = `It's ${number} day(s) left till ${specifiedWeekday}`;
        console.log(result)
        return result;
    }
}

// howFarIs('friday');
// howFarIs('Sunday');

// task 6
function isValidIdentifier(str) {
    let start = /./.test(str);
    let firstLetter = /^[a-z]/.test(str);
    let valid = /[a-zA-Z0-9_$]+$/g.test(str);
    let result = start && firstLetter && valid;
    console.log(result);
    return result;
}

// isValidIdentifier('myVar!'); // false
// isValidIdentifier('myVar$'); // true
// isValidIdentifier('myVar_1'); // true
// isValidIdentifier('1_myVar'); // false

// task 7
function capitalize(testStr) {
    let result = testStr.replace(/\b\w/g, a => a.toUpperCase());
    console.log(result);
    return result;
}

// const testStr = "My name is John Smith. I am 27.";
// capitalize(testStr); // "My Name! Is John Smith. I Am 27."

// task 8
function isValidAudioFile(fileName) {
    let validName = /^([a-zA-Z]+)((?!_).)*$/g.test(fileName);
    let validFormat = /\.(?:mp3|flac|alac|aac)$/g.test(fileName);
    let result = validName && validFormat;
    console.log(result);
    return result;
}

// isValidAudioFile('file.mp4'); // false
// isValidAudioFile('my_file.mp3'); // false
// isValidAudioFile('file.mp3'); // true

// task 9
function getHexadecimalColors(testString) {
    let result = [];
    let color = testString.match(/#([a-fA-F0-9]{6}|[a-fA-F0-9]{3})\b/g);
    if (color) {
        result = color;
    } else {
        result = [];
    }

    console.log(result);
    return result;
}

// const testString = "color: #3f3; background-color: #AA00ef; and: #abcd";
// getHexadecimalColors(testString); // ["#3f3", "#AA00ef"]
// getHexadecimalColors('red and #0000'); // [];

// task 10
function isValidPassword(password) {
    let valid = /^(?=.*?[A-Z])+(?=(.*[a-z])+)(?=(.*[\d])+).{8,}$/.test(password);
    console.log(valid);
    return valid;
}

// isValidPassword('agent007'); // false (no uppercase letter)
// isValidPassword('AGENT007'); // false (no lowercase letter)
// isValidPassword('AgentOOO'); // false (no numbers)
// isValidPassword('Age_007'); // false (too short)
// isValidPassword('Agent007'); // true

// task 11
function addThousandsSeparators(value) {
    let result = String(value).replace(/\d{1,3}(?=(\d{3})+(?!\d))/g, '$&,');
    console.log(result);
    return result;
}

// addThousandsSeparators("1234567890"); // "1,234,567,890"
// addThousandsSeparators(1234567890); // "1,234,567,890"

// task 12
function getAllUrlsFromText(text) {
    if (!text){
        return console.log('Error')    
    }
    
    let result = [];
    let regx = /(((http|https):\/\/)|www\.)+[-a-zA-Z0-9@:%._+~#=]+\.[a-z^0-9]{2,6}\b([-a-zA-Z0-9@:%_+.~#?&//=]*)/gm;
    let url = text.match(regx);
    if (url) {
        result = url;
        console.log(result);
        return result;
    } else {
        console.log(result);
        return result;
    }
}

const text1 = 'We use   https://translate.google.com/ to translate some words and phrases from https://angular.io/ ';
const text2 = 'JavaScript is the best language for beginners!'
getAllUrlsFromText(text1); // ["https://translate.google.com/", "https://angular.io/"]
getAllUrlsFromText(text2); // []
getAllUrlsFromText(); // (error)


